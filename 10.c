//Q10. Read at most 10 names of students and store them into an array of String nameOfStudents[10]. Sort the array and display them back. Hint: Use Arrays.sort() method.
//===============================================================================================================================================
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
int cmpfun(const void *a, const void * b)
{
    return(strcmp((char *)a,(char *)b));
}
int main()
{
    char nameOfStudents[5][50];
    for (int i = 0; i < 5; i++)
    {
        printf("Please enter the name of the students %d:",i+1);       
        {   
            gets(nameOfStudents[i]);
        }      
    }

   qsort(nameOfStudents,5,50,cmpfun);
   printf("After sort: \n");
   for (int i = 0; i < 5; i++)
    {
        printf("Name of the students %d:",i+1);
        puts(nameOfStudents[i]);
    }

    return 0;
    
}

//MULTIPLICATION OF TWO MATRIX USING POINTER
//======================================================================================================================================
#include<stdio.h>
void get_arr(int arr[3][3]);
void print_pro(int arr1[3][3],int arr2[3][3]);
int main()
{
   int arr1[3][3],arr2[3][3];  // Can declare globally as well.
   //int add[3][3],sub[3][3],pro[3][3];// no use to declare here.
   printf("please enter elements of matrix 1::\n");
   get_arr(arr1);
   printf("please enter elements of matrix 2::\n");
   get_arr(arr2);
   printf("Multiplication of two matrix is::\n");
   print_pro(arr1,arr2);

    return 0;
}
void get_arr(int arr[3][3])
{
    int i,j;
    for(i=0;i<3;i++)
 {
     for(j=0;j<3;j++)
     {
         scanf("%d",*(arr+i)+j);
     }
 }
}

void print_pro(int arr1[3][3],int arr2[3][3])
{
    int pro[3][3],i,j,sum=0;
for(i=0;i<3;i++)
{
    for(j=0;j<3;j++)
    {
        for (int k = 0; k < 3; k++)
        {
            sum= sum + arr1[i][k]*arr2[k][j];

        }
        pro[i][j]=sum;
        sum=0;
        
    }
}
for(i=0;i<3;i++)
    {
     for(j=0;j<3;j++)
     {
         printf(" %d ",*(*(pro+i)+j));
     }
     printf("\n");
    }

}

#include<stdio.h>
struct emp
{
    char first_name[10];
    char last_name[10];
    double sal;
};
void emp_init(struct emp* e);
void set_salary(struct emp *e, double sal);
void emp_display(struct emp *e);

int main()
{
    struct emp e1,e2; 
    double sal=0,sal2=0;
    emp_init(&e1);
    
    printf("Please enter the %s monthly salary: ",e1.first_name);
    scanf("%lf",&sal);
    set_salary(&e1,sal);
    emp_init(&e2);
    printf("Please enter the %s monthly salary: ",e2.first_name);
    scanf("%lf",&sal2);    
    set_salary(&e2,sal2);
    emp_display(&e1);
    emp_display(&e2);

    printf("==========================================After increment================================================\n");
    set_salary(&e1,1.1*sal);
    set_salary(&e2,1.1*sal2);
    printf("Monthly salary after increment is %lf\n",e1.sal);
    printf("Monthly salary after increment is %lf\n",e2.sal);
    return 0;
}

void emp_init(struct emp* e)
{
    printf("Please enter the employee first name:");
    scanf("%s",e->first_name);    
    printf("Please enter the employee last name:"); 
    scanf("%s",e->last_name);
}

void set_salary(struct emp *e, double sal)
{
    e->sal=sal;
}

void emp_display(struct emp *e)
{
    printf("Employee First Name is %s\n",e->first_name);
    printf("Employee last Name is %s\n",e->last_name);
    printf("Salary of %s %s on monthly basis is: %lf\n",e->first_name,e->last_name,e->sal);

}
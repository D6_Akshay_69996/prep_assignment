//Q9. Accept an integer number and when the program is executed print the binary, octal and hexadecimal equivalent of the given number.
//============================================================================================================================================
#include<stdio.h>
void OctalString(int num);
void BinaryString(int num);
void HexString(int num);    
int main()
{
    int num;
    printf("Please enter the number: ");
    scanf("%d",&num);
    printf("Given Number = %d\n",num);
    // printf("Equivalent octal number is = %o\n",num);
    // printf("Equivalent hexadecimal number is = %x\n",num);

    BinaryString(num);
    printf("\n");
    OctalString(num);
    printf("\n");
    HexString(num);    

    return 0;
}

void BinaryString(int num)
{
    int i=0;
    int a[8];
    for(i = 0; num>0; i++)
    {       
        a[i]=num%2;
        num=num/2;
    }
    for (i=i-1; i>=0; i--)
    {   
        printf("%d",a[i]);
    }

}

void HexString(int num)
{
    int h[8];
    int i;
    for (i = 0; num> 0; i++)
    {
        h[i] = num%16;
        num= num/16;
    }
    for ( i = i-1; i>=0; i--)
    {
        printf("%d",h[i]);
    }

}
void OctalString(int num)
{
    int o[8];
    int i;
    for (i = 0; num> 0; i++)
    {
        o[i] = num%8;
        num= num/8;
    }
    for ( i = i-1; i>=0; i--)
    {
        printf("%d",o[i]);
    }
}
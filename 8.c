//Q8. Write a program to read the name of a student (studentName), roll Number (rollNo) and marks (totalMarks) obtained. rollNo may be an alphanumeric string. Display the data as read. Hint: Create a Student structure and write appropriate functions.
//========================================================================================================================================
#include<stdio.h>

struct student
{
    int roll_number;
    char name[10];
    int marks;
};
void input(struct student *s1);

int main()
{
    struct student s1;
    // scanf("%s%d%d",s1.name,&s1.roll_number,&s1.marks);
    // printf("Name=%s\nRoll Number:=%d\n,Marks=%d",s1.name,s1.roll_number,s1.marks);
    input(&s1);
    
}


void input(struct student *s1)
{
    scanf("%s%d%d",s1->name,&s1->roll_number,&s1->marks);
    printf("Name=%s\nRoll Number:=%d\nMarks=%d",s1->name,s1->roll_number,s1->marks);
}
